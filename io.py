from Adafruit_7segment import SevenSegment as Display
import RPi.GPIO as GPIO
from time import time as now
from math import floor

class IO():
	# A list of all input pins
	pinin = {
		'main-button': -1,
		'rotary-encoder-button': 23,
		'rotary-encoder-1': 24,
		'rotary-encoder-2': 25,
		'ensure-button': -1,
		'quinne-button': -1,
		'lick-break': -1,
	}

	# A list of all output pins
	pinout = {
		'main-button-led': -1,
		'lick-led': -1,
		'ensure-solenoid': -1,
		'quinine-solenoid': -1,
	}

	# How long to wait to account for switch bouncing
	ms_wait = 100

	# Run IO initiation
	def __init__(self):
		self.init_io()

	# Initiate input output using GPIO
	def init_io(self):
		# We're going to use the pins on the Adafruit hat
		GPIO.setmode(GPIO.BCM)
		self._names = {}
		self._pressed = []
		self._last = []

		# Set up input pins as input. All need pullup resistors
		for i, key in enumerate(self.pinin):
			GPIO.setup(self.pinin[key], GPIO.IN, GPIO.PUD_UP)
			self._names[key] = i
			self._pressed.append(False)
			self._last.append(now)

		# Set up output
		for key, val in self.pinout:
			GPIO.setup(val, GPIO.OUT)

		# Set up display
		self._display = Display(address=0x70)
		self._displayt = -1

	# Determine whether a button has been pressed by catching rising 
	# or falling edges. Requires a signalt o be greater than ms_wait
	def _button_edge(self, button):
		but = self._names[button]

		# Button is not actively being pressed
		if GPIO.input(self.pinin[button]):
			# If we think it's being pressed and it's been long enough
			if self._pressed[but] and now() - self._last[but] > self.ms_wait:
				self._pressed[but] = False
				self._last[but] = now()

		# Button is actively being pressed
		else:
			# If we think it's not being pressed and it's been a while
			if not self._pressed[but] and now() - self._last[but] > self.ms_wait:
				self._pressed[but] = True
				self._last[but] = now()

		return self._pressed[but]

	# During training, simply check if the program needs to be stopped
	# Should be threaded
	def stop(self):
		return self._button_edge('main-button')

	# Turn on the clock and set the button to unpressed
	def start(self):
		self.display_time()
		but = self._names['main-button']
		self._pressed[but] = False
		self._last[but] = now()

	# Check if animal has licked
	def lick(self):
		return self._button_edge('lick-led')

	# List the state of all inputs
	def buttons(self):
		out = {}
		for key in self._names:
			out[key] = self._button_edge(key)
		return out

	# Set the state of an output
	def state(self, output, value):
		GPIO.output(self.pinout[output], value)

	# Turn output on
	def on(self, output):
		GPIO.output(self.pinout[output], True)

	# Turn output off
	def off(self, output):
		GPIO.output(self.pinout[output], False)

	# Display the current time in minutes: seconds
	def display_time(self):
		if self._displayt < 0:
			self._displayt = now()
			self._display.setColon(True)

		elapsed = now() - self._displayt
		minutes = int(floor(elapsed/60.0))
		seconds = int(floor(elapsed - minutes*60))

		min_tens = int(floor(minutes/10.0))
		min_ones = int(floor(minutes - min_tens*10))

		sec_tens = int(floor(seconds/10.0))
		sec_ones = int(floor(seconds - sec_tens*10))

		self.writeDigit(0, min_tens)
		self.writeDigit(1, min_ones)
		self.writeDigit(3, sec_tens)
		self.writeDigit(4, sec_ones)

	# Remove colons and display a number
	def display_number(self, val):
		if self._displayt >= 0:
			self.displayt = -1
			self._display.setColon(False)

		fours = int(floor(val/1000.0))
		threes = int(floor((val - fours*1000)/100.0))
		twos = int(floor((val - fours*1000 - threes*100)/10.0))
		ones = int(round(val - fours*1000 - threes*100 - twos*10))

		if val > 1000:
			self.writeDigit(0, fours)
		if val > 100:
			self.writeDigit(1, threes)
		if val > 10:
			self.writeDigit(3, twos)
		self.writeDigit(4, ones)

# Generate an IO instance
def io():
	out = IO()
	return out


if __name__ == '__main__':
	print("Here we go! Press CTRL+C to exit")
	try:
		while 1:
			pass
#			if GPIO.input(butPin): # button is released
#				GPIO.output(ledPin, GPIO.LOW)
#			else: # button is pressed:
#				pwm.ChangeDutyCycle(100-dc)
#				GPIO.output(ledPin, GPIO.HIGH)
#				time.sleep(0.075)
#				GPIO.output(ledPin, GPIO.LOW)
#				time.sleep(0.075)
	except KeyboardInterrupt: # If CTRL+C is pressed, exit cleanly:
		GPIO.cleanup() # cleanup all GPIO