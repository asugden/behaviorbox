import os, os.path as opath
from parser import Parser
from copy import deepcopy
from random import random
import numpy as np

class Program():
	progroot = 'programs'
	movroot = ' movies'

	def_pars = {
		'movie': '',
		'trial-length': [0.0, 0.0],
		'quit-after': 0,
		'reward-after': [0.0, 0.0],
	}

	def_stim = {
		'movie-time': [0.0, 0.0],
		'probability': 0.0,
		'solenoid': -1,
		'window': [0.0, 0.0],
	}

	progtypes = []
	programs = []
	trials = []

	pars = {}

	def __init__(self):
		self.parse = Parser()
		self.reload()

	def load(self, prg):
		self.pars = {}
		self.trials = []
		self._readProgram(prg)
		#self._createProgram()

	def _readProgram(self, prg):
		fp = open(opath.join(self.progroot, prg + '.txt'))
		data = fp.read().split('\n')
		fp.close()

		self.pars = deepcopy(self.def_pars)
		stim = ''
		stimpars = {}

		# Iterate through each line in the parameter file
		for line in data:
			# Skip blank and comment lines
			if len(line) > 0 and line[0].strip() != '#':
				# Split the line and check for indents
				keyval = line.split(':')
				lspaces = len(keyval[0]) - len(keyval[0].lstrip())

				# Check if general parameters or stim-specific parameters
				if keyval[0].strip() == 'stims': stim = 'stim'
				elif len(stim) > 0 and lspaces == 0:
					stim = ''
					self._addStim(stimpars)
					stimpars = {}
				elif len(stim) > 0 and lspaces == 1:
					stim = keyval[0].strip()
					if len(keyval[1].strip()) > 0:
						stimpars[stim] = keyval[1].strip()
				elif len(stim) > 0 and lspaces == 2:
					if stim not in stimpars:
						stimpars[stim] = deepcopy(self.def_stim)
					if keyval[0].strip() in stimpars[stim]:
						stimpars[stim][keyval[0].strip()] = self.parse.unknown(keyval[1])
				else:
					if keyval[0].strip() in self.pars:
						self.pars[keyval[0].strip()] = self.parse.unknown(keyval[1])
		# Check for files with no newlines at the end
		if len(stimpars) > 0: self._addStim(stimpars)

	def _addStim(self, stim):
		types = [key for key in stim if key != 'trials']
		if 'trials' not in stim or len(types) == 0:
			print '\tWARNING: Trials not in stim or poorly formed'
			return False

		probs = np.array([stim[key]['probability'] for key in types])
		probs = probs/np.sum(probs)
		probs = np.cumsum(probs)

		for key in types:
			stim[key]['stimulus'] = key

		for i in range(int(stim['trials'])):
			self.trials.append(stim[types[np.argmax(probs >= random())]])

	def reload(self):
		"""
		Get a list of all programs and program types (which are defined
		as the two-letter pair at the beginning of the program).
		"""
		self.progtypes = []
		self.programs = []

		fs = os.listdir(self.progroot)
		for f in fs:
			if opath.splitext(f)[1] == '.txt':
				pname = opath.splitext(opath.split(f)[1])[0]
				ptype = pname[:2]
				self.programs.append(pname)
				if ptype not in self.progtypes:
					self.progtypes.append(ptype)

		self.progtypes.sort()
		self.programs.sort()

		return self

	def getProgramTypes(self):
		"""Return a list of all 2-letter program prefixes."""
		return self.progtypes

	def getPrograms(self, ptype=''):
		"""
		Get a list of all programs or programs matching a specific
		2-letter prefix code with optional ptype argument.
		"""
		if len(ptype) > 0:
			return [p for p in self.programs if p[:2] == ptype]
		else:
			return self.programs



	# def program_range(self):
	# 	"""
	# 	Return the maximum program number.
	# 	"""

	# 	fs = os.listdir(self.progroot)
	# 	print fs


	# 	nums = [int(opath.splitext(opath.split(f)[1])[1])
	# 		for f in fs if opath.splitext(f)[1] == '.txt']
	# 	nums.sort()
	# 	return nums[-1]


if __name__ == '__main__':
	pr = Program()
	pr.load('as-02')
	print(pr.trials)
	#print pr.pars
	#print pr.stims
	#print pr.getProgramTypes()
	#print pr.getPrograms('as')
