# Path to the movie containing all stimuli
movie: moviesMov_141.avi
trial-length: [8.0, 12.0]

# Adaptive parameters- quit after so many ignroed stimuli and add a 
# pavlovian trial if the animal has not been rewarded with a linear 
# probability between the first and second values
# IGNORED AS OF NOW
quit-after: 0
reward-after: [0, 0]

# Stimulus parameters, when the stimulus is in the movie, its 
# probability, the result of licking (reward solenoid, punishment
# solenoid, or neither), and a guaranteed outcome for 
stims:
	trials: 20
	pavlov:
		movie-time: [0, 2]
		probability: 0.2
		solenoid: reward
		window: [0, 2]

stims:
	trials: 180
	plus: 
		movie-time: [0, 2]
		probability: 0.8
		solenoid: reward
		window: [2, 4]
	pavlov:
		movie-time: [0, 2]
		probability: 0.2
		solenoid: reward
		window: [0, 2]