from arduinostate import ArduinoState
from program import Program

from char_lcd.Adafruit_CharLCD import Adafruit_CharLCD
from char_lcd.Adafruit_MCP230XX import MCP230XX_GPIO

import time
import RPi.GPIO as GPIO

class BehaviorBox():
	mode = 0 # 0 is checking for input, 1 is running
	start = -1

	disp_state = {
		'progtype': '',
		'program': '',
		'on': 0,
	}

	def __init__(self):
		self.prog = Program()
		self.ard = ArduinoState()
		self.mcp = MCP230XX_GPIO(1, 0x20, 8)
		self.lcd = Adafruit_CharLCD(pin_rs=1, pin_e=2, pins_db=[3,4,5,6], GPIO=self.mcp)
		self._baseProgram()

	def run(self):
		if self.start < 0:
			self.inputs()
		else:
			self.stim()

	def inputs(self):
		vals = self.ard.getio()
		if vals['click']:
			self.disp_state['on'] = (self.disp_state['on'] + 1)%2
			self.redisplay(0)
		elif vals['rotate'] != 0:
			self.redisplay(int(vals['rotate']))

	def redisplay(self, rotation):
		ln2 = '\n-set number' if self.disp_state['on'] == 0 else '\n-set name'
		if rotation != 0 and self.disp_state['on'] == 0:
			pts = self.prog.getProgramTypes()
			if rotation > 0:
				pn = (pts.index(self.disp_state['progtype']) + 1)%len(pts)
			else:
				pn = (pts.index(self.disp_state['progtype']) - 1)%len(pts)
			self.disp_state['progtype'] = pts[pn]
		elif rotation != 0 and self.disp_state['on'] == 1:
			prs = self.prog.getPrograms(self.disp_state['program'])
			print prs, self.disp_state['program']
			if rotation > 0:
				pn = (prs.index(self.disp_state['program']) + 1)%len(prs)
			else:
				pn = (prs.index(self.disp_state['program']) - 1)%len(prs)
			self.disp_state['program'] = prs[pn]

		self.lcd.clear()
		self.lcd.message(self.disp_state['program'].upper() + ln2)


	def _baseProgram(self):
		self.lcd.clear()
		self.disp_state['progtype'] = self.prog.getProgramTypes()[0]
		self.disp_state['program'] = self.prog.getPrograms()[0]
		self.lcd.message(self.disp_state['program'].upper() + '\n-set name')









if __name__ == '__main__':
	bb = BehaviorBox()
	while True:
		bb.run()