import smbus
import time

class ArduinoState():
	aio = 0x05
	aopt = 0x04
	disp = 0x20
	disp_delay = 250

	iostate = {
		'led': True,
		'servo': False,
		'solenoid': -1,
		'solenoid-delay': 0,
		'solenoid-on': 250,
		'last-push': -1,
	}

	def __init__(self):
		self.bus = smbus.SMBus(1)
		self.setup()

	def setio(self):
		self.bus.write_i2c_block_data(self.aio,
			1 if self.iostate['led'] else 0, [
			self.iostate['solenoid'],
			int(self.iostate['solenoid-on']/10.0),
			self.iostate['solenoid-delay'],
			self.iostate['servo'],
			])
		self.iostate['last-push'] = time.time()*1000.0

	def getio(self):
		d = self.bus.read_i2c_block_data(self.aio, 0);
		return {
			'licks': d[0] << 8 | d[1],
			'keylick': self.iostate['last-push'] + float((((d[2] << 8) | d[3] << 8) | d[4] << 8) | d[5]),
			'rotate': int(d[6]) - 128,
			'click': True if d[7] > 0 else False,
		}

	def setup(self):
		self.iostate['led'] = True
		self.iostate['solenoid'] = -1
		self.setio()

	def iti(self):
		self.iostate['led'] = False
		self.iostate['solenoid'] = -1
		self.setio()

	def pavlov(self):
		self.iostate['led'] = False
		self.iostate['solenoid'] = 0
		self.iostate['solenoid-delay'] = self.disp_delay
		self.setio()

	def cs(self, csn):
		translation = {'plus':0, 'minus':1, 'other':2}
		self.iostate['led'] = False
		self.iostate['solenoid'] = translation[csn]
		self.iostate['solenoid-delay'] = 0
		self.setio()